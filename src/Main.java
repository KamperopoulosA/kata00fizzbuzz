public class Main {
    public static void main(String[] args) {
        //loop from number 1 to 100
        for(int i=1; i<=100; i++){

            //numbers divisible by both 3 and 5
            if(i%3 == 0 && i%5 ==0){
                System.out.print("FizzBuzz, ");
                continue;
            }

            //numbers divisible by 3
            if(i%3 == 0){
                System.out.print("Fizz, ");
                continue;
            }

            //numbers divisible by 5
            if(i%5 == 0){
                System.out.print("Buzz, ");
                //continue;
            }

            else
                System.out.print(i+", ");
        }
    }
}